from requests import post
from properties import BASE_URL, HEADERS

data = {"entity_id": "light.kantoor"}

response = post(BASE_URL + "services/light/toggle", headers=HEADERS, json=data)
print(response.text)