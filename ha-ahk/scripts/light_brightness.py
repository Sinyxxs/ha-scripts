import sys
from requests import post
from properties import BASE_URL, HEADERS

operation = sys.argv[1]

if operation == "decrease":
	brightness_step = -25
elif operation == "increase":
	brightness_step = 25

data = {
	"entity_id": "light.kantoor", 
	"brightness_step": brightness_step
}

response = post(BASE_URL + "services/light/turn_on", headers=HEADERS, json=data)
print(response.text)