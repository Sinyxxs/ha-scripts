#NoTrayIcon
^+!l::
{
	Run "C:\AHK\ha-ahk\scripts\light_toggle.py",, Hide
	return
}
^+!NumpadAdd::
{
	RunWait "C:\AHK\ha-ahk\scripts\light_brightness.py" "increase",, Hide
	return
}
^+!NumpadSub::
{
	RunWait "C:\AHK\ha-ahk\scripts\light_brightness.py" "decrease",, Hide
	return
}
